const techs = {
    // essential
    fileProvider: require('enb/techs/file-provider'),
    fileMerge: require('enb/techs/file-merge'),

    // optimization
    borschik: require('enb-borschik/techs/borschik'),

    // css
    stylus: require('enb-stylus/techs/stylus'),

    // js
    browserJs: require('enb-js/techs/browser-js'),

    // bemhtml
    bemhtml: require('enb-bemxjst/techs/bemhtml'),
    bemjsonToHtml: require('enb-bemxjst/techs/bemjson-to-html')
};

const enbBemTechs = require('enb-bem-techs');

const levels = [
    { path: 'libs/bem-core/common.blocks', check: false },
    { path: 'libs/bem-core/desktop.blocks', check: false },
    { path: 'libs/bem-components/common.blocks', check: false },
    { path: 'libs/bem-components/desktop.blocks', check: false },
    { path: 'libs/bem-components/design/common.blocks', check: false },
    { path: 'libs/bem-components/design/desktop.blocks', check: false },
    { path: 'libs/bem-history/common.blocks', check: false },
    'common.blocks',
    'desktop.blocks'
];

module.exports = function(config) {
    const isProd = process.env.YENV === 'production';

    // Привязка инструментов обработки (технологий) к целям (таргетам)
    config.nodes('*.bundles/*', (nodeConfig) => {
        // Регистрация технологий
        nodeConfig.addTechs([
            // essential

            /** собирает информацию об уровнях переопределения проекта. Результат выполнения этой технологии необходим технологиям:
             - `enb/techs/deps`
             - `enb/techs/files`.
             Для каждой ноды по умолчанию добавляется уровень `<путь_к_ноде>/blocks`.
             Например, для ноды `pages/index` — `pages/index/blocks`
             */
            [enbBemTechs.levels, { levels: levels }],

            /** сообщает make-платформе, что таргет (переданный в опции `target`) уже готов. В нашем случае, исходным файлом для сборки является `?.bemjson.js`. Он лежит в репозитории и отдельная сборка для него не требуется.
             */
            [techs.fileProvider, { target: '?.bemjson.js' }],

            /** index.bemdecl.js */
            [enbBemTechs.bemjsonToBemdecl, {
                source: '?.bemjson.js'
            }],

            /** собирает `?.deps.js` (`index.deps.js`) на основе `index.bemdecl.js` и `index.levels` */
            [enbBemTechs.deps],

            /**
             собирает полный список файлов со всех уровней переопределения в том порядке, в котором они идут в финальном `index.deps.js`. Результат этой технологии может использоваться, например, в технологии `enb/techs/js`
             */
            [enbBemTechs.files],

            // css
            // https://github.com/enb/enb-stylus/blob/d87196ac60b952c83e7a94a09691d604bc018f02/api.ru.md
            [techs.stylus, {
                // Unfreeze версия передаётся в задачу freeze
                target: '?.unfreeze.css',
                // Суффиксы, по которым отбираются файлы стилей для дальнейшей сборки.
                sourceSuffixes: ['styl', 'css'],
                sourcemap: false,
                // Oбработка url() внутри файлов .styl и .css
                // inline: содержимое файла будет закодировано в base64
                // rebase: изменение пути к содержимому относительно таргета
                url: 'inline',
                // Максимальный размер файла в килобайтах, который будет закодирован в base64 в режиме inline.
                inlineMaxSize: 14,
                // https://github.com/postcss/autoprefixer#options
                autoprefixer: {
                    browsers: ['ie >= 10', 'last 2 versions', 'opera 12.1', '> 2%']
                },
                // Обрамление комментариями CSS-кода в собранном файле. Комментарии cодержат относительный путь до исходного файла. Может быть использовано при разработке проекта.
                comments: true,
                // Подключает .styl-файлы с глобальными переменными, методами или миксинами в начало
                // На данный момент миксины и глобалы подключаются к каждому файлу-стилей индивидуально через import
                globals: [
                    '../../common.blocks/page/globals.styl'
                ]
            }],

            // handle bemhtml files
            // https://github.com/enb/enb-bemxjst/blob/master/api.ru.md
            [techs.bemhtml, {
                // Имя скомпилированного файла, куда будет записан результат сборки необходимых bemhtml.js-файлов проекта.
                target: '?.bemhtml.js',
                // Суффиксы файлов, по которым отбираются файлы BEMHTML-шаблонов для дальнейшей сборки.
                sourceSuffixes: ['bemhtml', 'bemhtml.js'],
                // Задает имена или пути для подключения сторонних библиотек.
                // После подключения сторонней библиотеки ее можно использовать в шаблонах с помощью метода this.require
                requires: {},
                engineOptions: {
                    naming: {
                        elem: '__',
                        mod: '_'
                    }
                }
            }],

            // convert to html files from bemjson
            [techs.bemjsonToHtml, {
                target: '?.html',
                bemhtmlFile: '?.bemhtml.js'
            }],

            // client bemhtml
            [enbBemTechs.depsByTechToBemdecl, {
                target: '?.bemhtml.bemdecl.js',
                sourceTech: 'js',
                destTech: 'bemhtml'
            }],
            [enbBemTechs.deps, {
                target: '?.bemhtml.deps.js',
                bemdeclFile: '?.bemhtml.bemdecl.js'
            }],
            [enbBemTechs.files, {
                depsFile: '?.bemhtml.deps.js',
                filesTarget: '?.bemhtml.files',
                dirsTarget: '?.bemhtml.dirs'
            }],
            [techs.bemhtml, {
                target: '?.browser.bemhtml.js',
                filesTarget: '?.bemhtml.files',
                sourceSuffixes: ['bemhtml', 'bemhtml.js']
            }],

            // js
            [techs.browserJs, { includeYM: true }],

            /**
             * *String[]* **sources** — Список исходных таргетов.
             * *String* **target** — Результирующий таргет.
             * *String* **divider** — Строка для склеивания файлов. По умолчанию — "\n".
             * *Boolean* **sourcemap** — Построение карт кода (source maps) с информацией об исходных файлах.
             */
            [techs.fileMerge, {
                target: '?.js',
                sources: ['?.browser.js', '?.browser.bemhtml.js']
            }],

            // https://github.com/borschik/borschik/blob/master/docs/freeze/freeze.ru.md#Заморозка-freeze-статических-ресурсов
            // задача stylus инлайнит небольшие картинки,
            //   а эта задача фризит оставшиеся картинки
            [techs.borschik, {
                source: '?.unfreeze.css',
                target: '?.css',
                freeze: true,
                minify: false
            }],

            // optimization: minify, uglify
            [techs.borschik, { source: '?.js',
                               target: '?.min.js',
                               minify: isProd }],
            [techs.borschik, { source: '?.css',
                               target: '?.min.css',
                               minify: isProd }]
        ]);

        // Объявим таргеты, которые надо собрать для ноды
        // Target (таргет) — это цель для сборки.
        // Например, `index.js` в рамках ноды `pages/index`
        // Masked Target (замаскированный таргет) —
        //   это имя таргета, которое может содержать `?`.
        // Знак `?` заменяется на имя ноды в процессе настройки технологии,
        // а с помощью подстроки `{lang}` можно создать несколько копий технологии для каждого из языков, где `{lang}` заменится на аббревиатуру языка в каждой из копий технологии.
        // Например, таргет `?.js` заменяется на `search.js`,
        //   если нода — `pages/search`.
        // Такой подход удобен при настройке нескольких нод через `nodeMask`
        nodeConfig.addTargets([
            '?.html',
            '?.min.css',
            '?.min.js'
        ]);
    });
};
