module.exports = {
    block: 'page',
    title: 'Черехапа страхование',
    head: [
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'view.min.css' }
    ],
    scripts: [{ elem: 'js', url: 'view.min.js' }],
    content: [
        {
            block: 'header',
        },
        {
            block: 'view-router',
            content: [
                {
                    block: 'view',
                    name: 'home',
                    title: 'Home Cherehapa',
                    header: true,
                    footer: true,
                    content: [
                        {
                            tag: 'h1',
                            content: 'Туристическая страховка'
                        },
                        {
                            tag: 'p',
                            content: 'Мы уникальный сервис по сравнению условий страхования в крупнейших страховых компаниях. Мы переработали правила страхования всех страховых компаний и даем вам возможность выбрать лучшие условия страхования для своего путешествия удобным, понятным и простым способом.'
                        }
                    ]
                },
                {
                    block: 'view',
                    name: 'about',
                    title: 'About Cherehapa',
                    header: false,
                    footer: false,
                    content: [
                        {
                            tag: 'h1',
                            content: 'Кто такой Черехапа?'
                        },
                        {
                            tag: 'p',
                            content: 'Cherehapa — это онлайн сервис по выбору и покупке страховки для путешествия. Мы помогаем подобрать оптимальный вариант страхования, сравнить предложения крупнейших компаний и купить электронный полис за 7 минут. Мы знаем особенности поездок в разные страны и даем вам советы и подсказки.'
                        }
                    ]
                },
                {
                    block: 'view',
                    name: 'faq',
                    title: 'FAQ Cherehapa',
                    content: [
                        {
                            tag: 'h1',
                            content: 'Вопросы и ответы'
                        },
                        {
                            tag: 'article',
                            content: [
                                {
                                    tag: 'h2',
                                    content: 'Какая страховая компания лучше?'
                                },
                                {
                                    tag: 'p',
                                    content: 'Мы предоставляем услуги крупнейших страховых компаний: ERV, Liberty, АльфаСтрахование, ВТБ Страхование, Зетта Страхование, Ингосстрах, Опора Страхование, Ренессанс, Росгосстрах, Русский Стандарт. Мы работаем только с проверенными страховыми компаниями, которые предоставляют отличный сервис и следят за качеством обслуживания клиентов, поэтому вы можете быть уверены в своем выборе.'
                                }
                            ]
                        }
                    ]
                },
                {
                    block: 'view',
                    title: '404 Cherehapa',
                    footer: true,
                    content: [
                        {
                            tag: 'article',
                            content: [
                                {
                                    tag: 'h1',
                                    content: 'По запрошенному адресу страницы на сайте нет'
                                },
                                {
                                    tag: 'h2',
                                    content: 'Почему?'
                                },
                                {
                                    tag: 'ul',
                                    content: '<li>Вероятно, Вам дали неверную ссылку или Вы набрали неточный адрес.</li><li>Возможно, такая страница существовала ранее, но информация на ней устарела и страница была удалена.</li>'
                                },
                                {
                                    tag: 'h2',
                                    content: 'Что делать?'
                                },
                                {
                                    tag: 'ul',
                                    content: '<li>начните с главной страницы сайта;</li><li>нажимте кнопку «Назад» Вашего браузера;<li>обратитетсь с сотрудникам по телефону.</li>'
                                }

                            ]
                        }
                    ]
                }

            ]
        },
        {
            block: 'footer'
        },
    ]
};
