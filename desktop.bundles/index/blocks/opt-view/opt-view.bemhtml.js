block('opt-view')(
    content()([{
        elem: 'first',
        content: 'Медицинская страховка на сумму'
    }, {
        elem: 'second',
        content: 'Дополнительные опции'
    }, {
        elem: 'final',
        content: {
            block: 'flat-link',
            url: '#start',
            content: 'Вернуться на главную'
        }
    }])
);
