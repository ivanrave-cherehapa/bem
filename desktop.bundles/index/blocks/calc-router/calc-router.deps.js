({
    shouldDeps: [
        { block: 'start-view' },
        { block: 'start-view', mods: { visible: true } },
        { block: 'opt-view' },
        { block: 'opt-view', mods: { visible: true } },
        { block: 'countries-view' },
        { block: 'countries-view', mods: { visible: true } },
        { block: 'depart-view' },
        { block: 'depart-view', mods: { visible: true } },
        { block: 'backward-view' },
        { block: 'backward-view', mods: { visible: true } },
        { block: 'notfound-view' },
        { block: 'notfound-view', mods: { visible: true } }
    ]
});
