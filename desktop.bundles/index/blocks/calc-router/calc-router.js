/**
 * @param {String} paramString Url parameters as a string, after ? sign
 * @returns {Object} params
 */
const calcUrlParams = function(paramString) {
    const obj = {};
    if (!paramString) {
        return obj;
    }

    const parts = paramString.split('&');
    parts.forEach((part) => {
        if (part) {
            const arr = part.split('=');
            obj[arr[0]] = arr[1];
        }
    });
    return obj;
};

/**
 * Роутер на уровне корня калькулятора.
 * Роутер содержит элементы (представления), которые скрываются
 *   или отображаются в зависимости от входных данных.
 * Блоки, входящие в содержимое представлений, не должны быть привязаны
 *   к роутеру: поэтому скрываются/отображаются именно элементы роутера.
 *
 * Входные данные:
 * - хэш из адресной строки (в текущей реализации)
 * - полноценный адрес (альтернатива, используя historyAPI)
 *
 * Только на уровне роутера определяются параметры адресной строки, внутренние компоненты не должны напрямую обращаться к глобальным объектам.
 * При каждом изминении адреса роутер передаёт обновлённые параметры выбранному представлению через специальный метод представления: activate
 * Представление показывается только после обработки входных параметров.
 */
modules.define('calc-router', ['i-bem__dom'], function(provide, BEMDOM) {
    provide(BEMDOM.decl(this.name, {
        views: {
            start: 'start-view',
            opt: 'opt-view',
            countries: 'countries-view',
            depart: 'depart-view',
            backward: 'backward-view',
            notfound: 'notfound-view'
        },
        onSetMod: {
            js: {
                inited: function() {
                    window.onhashchange = (ev) => {
                        const fullHash = ev.newURL.split('#')[1];

                        this.updateView(this.defineView(fullHash));
                    };

                    this.updateView(this.defineView(window.location.hash.substr(1)));
                }
            }
        },
        /**
         * Validate a path and select a view
         * @private
         * @param {String} fullHash URL hash without #
         * @returns {Object} View name + view params
         */
        defineView: function(fullHash) {
            if (!fullHash) {
                return {
                    view: this.views.start,
                    params: {}
                };
            }

            // separate parameters
            const parts = fullHash.split('?');
            const routePath = parts[0] || '';
            const routeParams = calcUrlParams(parts[1]);

            if (this.views[routePath]) {
                return {
                    view: this.views[routePath],
                    params: routeParams
                };
            }

            return {
                view: this.views.notfound,
                params: routeParams
            };
        },
        /**
         * Show a required view, hide other views
         * @private
         * @param {Object} routeObj View name + params
         * @returns {undefined}
         */
        updateView: function(routeObj) {
            // hide all views, except required
            Object.keys(this.views).forEach((vk) => {
                const vn = this.views[vk];
                if (vn !== routeObj.view) {
                    const someView = this.findBlockInside(vn);
                    if (someView) {
                        if (someView.deactivate) {
                            someView.deactivate();
                        } else {
                            // just hide the view, if no deactivation logic
                            someView.delMod('visible');
                        }
                    }
                }
            });

            // show a required view
            const needView = this.findBlockInside(routeObj.view);
            if (needView) {
                // send url parameters to the view
                if (needView.activate) {
                    needView.activate(routeObj.params);
                } else {
                    // just show the view, if no activation logic
                    needView.setMod('visible');
                }
            } else {
                // todo: system error view
                throw new Error('No such view in DOM');
            }
        }
    }));
});
