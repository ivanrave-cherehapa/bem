block('calc-router')(
    js()(true),
    content()([{
        // начальная форма ввода
        block: 'start-view'
    }, {
        // опции страховки
        block: 'opt-view'
    }, {
        // выбор стран
        block: 'countries-view'
    }, {
        // отправление
        block: 'depart-view'
    }, {
        // возвращение
        block: 'backward-view'
    }, {
        // не найдена страница
        block: 'notfound-view'
    }])
);
