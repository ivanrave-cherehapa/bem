/** A simple link with href without js handlers */
block('flat-link')(
    tag()('a'),
    attrs()(function() {
        return { href: this.ctx.url };
    })
);
