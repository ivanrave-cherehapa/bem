// todo: показывать прогресс, пока грузятся данные
// todo: обработать ошибки АПИ
// todo: не найдено, если нет записей
// todo: управление клавишами: вниз, вверх, пробел
// todo: выделить группы, обособить сверху
// todo: учитывать уже выбранные страны
// todo: дополнять страны выбранной страной при передаче назад
// todo: подгружать данные только после показа представления, а не во время инициализации

/**
 * Сами по себе компоненты не могут исчезать и появлятся.
 * Этим занимается родительский компонент:
 * - создание
 * - обновление (удаление + создание)
 * - удаление
 * Скрытие (отображение) элемента - это не удаление (создание).
 * Вместо скрытия элемент может просто поменять цвет фона (например серый)
 * Родительский компонент вызывает на необходимых потомках смену состояния
 * Например при изменении фильтрующей строки - скрывает (затемняет) некоторые строки.
 *
 * Каждый элемент массива данных связан с представлением: dom element.
 * При изменении в массиве данных - вызывается обновление представления.
 * Данные отражаются на представлении.
 */
modules.define('country-list', ['i-bem__dom', 'api', 'bm'], function(provide, BEMDOM, api, bm) {
    const BLOCK_NAME = this.name;

    /**
     * ViewModel for country data
     * @param {Object} item Country item from API
     * @param {Boolean} isDisabled Whether the country is selected already
     * @param {Function} onCountrySelect Callback for country selection
     */
    class CountryRow {
        constructor(item, isDisabled, onCountrySelect) {
            this.id = item.id;
            this.name = item.name;
            this.search = item.search;

            /**
             * Class for this element
             * @private
             * @type {String}
             */
            this.cls = bm.elem(BLOCK_NAME, 'row');

            /**
             * A DOM object for this element
             * @private
             * @type {Object}
             */
            this.node = this.build(onCountrySelect, isDisabled);
        }
        /**
         * Template for a country item (in a list)
         * @private
         * @param {Function} onCountrySelect Callback for country selection
         * @param {Boolean} isDisabled Whether the item already choosen
         * @returns {Object} DOM element
         */
        build(onCountrySelect, isDisabled) {
            const e = document.createElement('div');
            e.className = this.cls;
            if (isDisabled) {
                e.classList.add(bm.mod(this.cls, 'disabled'));
            } else {
                e.addEventListener('click', () => onCountrySelect(this.id));
            }
            e.textContent = this.name;
            return e;
        }
        /**
         * Изменение состояния строки:
         * - скрытие элемента
         * - затемнение элемента
         * - и т.п.
         * @private
         * @param {String} val Value to filter
         * @return {undefined}
         */
        turnRow(val) {
            // console.log('this.elem(row)', this.elem('row'));
            if (this.search.toLowerCase().indexOf(val) >= 0) {
                this.node.classList.remove(bm.mod(this.cls, 'filtered'));
            } else {
                this.node.classList.add(bm.mod(this.cls, 'filtered'));
            }
        }
    }

    provide(BEMDOM.decl(this.name, {
        /**
         * Array of country viewmodels
         * @type {Array}
         */
        arrCountry: [],
        /**
         * Render the list
         * @public
         * @param {Array} arr - Array of countries (and groups)
         * @param {Array<String>} selectedCountryIds Ids
         * @returns {undefined}
         */
        render: function(arr, selectedCountryIds) {

            // скрывать уже выбранные страны
            // также возможно их деактивировать, вместо скрытия
            // replace an array with new viewmodels
            this.arrCountry = arr.map((item) => {
                const isEnabled = selectedCountryIds.indexOf(item.id) >= 0;
                return new CountryRow(item, isEnabled, this.selectCountry.bind(this, selectedCountryIds));
            });

            // Добавление блоков в текущий блок
            const wrap = this.domElem;
            // очистка предыдущего списка для замены содержимого
            wrap.empty();
            // append country blocks to the list
            this.arrCountry.forEach((country) => {
                wrap.append(country.node);
            });
        },
        /**
         * Click by country item and select it
         * - add to the selected list
         * - mark as a selected
         * - redirect to prev page with selected variation
         * Обмен данными между роутами можно осуществлять через
         * - параметры адресной строки (работает по всему вэбу)
         * - локальное хранилище (работает внутри сайта)
         * - внутренние переменные (работает в одностраничном)
         * @private
         * @param {Array<String>} selectedCountryIds Selected ids
         * @param {String} countryId Id of a selected country
         * @returns {undefined}
         */
        selectCountry: function(selectedCountryIds, countryId) {
            // если решим передавать состояние по адресной строке,
            // то можно использовать просто ссылки без хэндлеров
            // прим: многие интернет-магазины содержат кучу параметров в адресной строке, это позволяет задавать любое начальное состояние проекту, например дать ссылку пользовалю с уже выбранными странами
            const cids = selectedCountryIds.concat(countryId).join('__');
            window.location.hash = `#start?cids=${cids}`;
        },
        /**
         * Filter a list with countries
         * @public
         * @param {String} val Country name/translit to filter
         * @returns {undefined}
         */
        filterList: function(val) {
            // значение преобразовывается в нижний регистр для фильтрации
            const lower = val.toLowerCase();

            // console.log('elemrows', this.elem('row'));
            this.arrCountry.forEach((country) => {
                country.turnRow(lower);
            });
        }
    }));
});
