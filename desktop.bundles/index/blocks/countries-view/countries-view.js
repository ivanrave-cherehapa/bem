/**
 * Представление: выбор стран
 * Когда пользователь переключается на данный блок (по ссылке),
 *   подгружает данные и определяет, какие блоки отображать:
 * - список + инпут (при успешной загрузке)
 * - блок с ошибкой (при ошибочной загрузке)
 * - блок с прогрессом (если подгрузка данных идёт больше 300мс)
 */
modules.define('countries-view', ['i-bem__dom', 'api'], function(provide, BEMDOM, api) {
    provide(BEMDOM.decl(this.name, {
        /**
         * Show this view with new params
         * @param {Object} opt Parameters, usually from URL
         * @returns {undefined}
         */
        activate: function(opt) {
            // скрыть все предыдущие вью
            // показать прогресс

            api.getCountries()
            // скрыть прогресс
            // показать список + инпут
                .then(arr => this.handleSuccess(arr, opt))
            // скрыть прогресс
            // показать блок с ошибкой
                .catch(err => this.handleError(err));
        },
        /**
         * Hide this view
         * probable with destroying
         * @returns {undefined}
         */
        deactivate: function() {
            this.delMod('visible');
        },
        handleSuccess: function(arr, opt) {
            // init values
            // country-input - in shouldDeps
            const countryInput = this.findBlockInside('country-input');
            if (countryInput) {
                // clean the input field or set predefault value
                countryInput.cleanInput();
            }

            const countryList = this.findBlockInside('country-list');
            if (countryList) {
                const countryIds = opt.cids ? opt.cids.split('__') : [];
                countryList.render(arr, countryIds);
            }

            // visibility after construction
            this.setMod('visible');
        },
        /**
         * Обработка ошибки загрузки данных
         * показывает уведомление об ошибке
         * @private
         * @param {Object} err API error
         * @returns {undefined}
         */
        handleError: function(err) {
            // todo: блок с ошибкой
            alert('error: ' + err.msg);

            // console.log('catchCountries', err);

            // const div = document.createElement('div');
            // div.className = bm.elem(BLOCK_NAME, 'err');
            // div.textContent = err.msg;

            // // Добавление блоков в текущий блок
            // // очистка предыдущего списка для замены содержимого
            // this.domElem.empty().append(div);
        }
    }));
});
