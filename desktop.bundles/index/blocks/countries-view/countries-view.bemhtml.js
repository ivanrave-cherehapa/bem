block('countries-view')(
    js()(true),
    content()([
        {
            block: 'country-input',
            js: { target: 'country-list' }
        },
        {
            block: 'country-list'
        }
    ])
);
