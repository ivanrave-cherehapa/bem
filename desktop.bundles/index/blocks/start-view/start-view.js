/*
 * Начальный экран фильтр
 * - страны
 * - даты
 * - опции страховки
 * - данные путешественников
 *   - дата рождения (возраст) - также можно хранить в адресной строке
 *   - имя, фамилия (лучше заполнить потом при окончательном выборе страховки)
 * - кнопку для перехода к калькулятору
 * - кнопка: "очистить всё"
 */
modules.define('start-view', ['i-bem__dom', 'api'], function(provide, BEMDOM, api) {
    provide(BEMDOM.decl(this.name, {
        /**
         * Show this view with new params
         * @public
         * @param {Object} opt Parameters, usually from URL
         * @returns {undefined}
         */
        activate: function(opt) {
            // console.log('selected country ids', opt.cids,
            //           opt.depart, opt.backward);

            // convert string to array
            // reserved characters = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" | "$" | ","
            const countryIds = opt.cids ? opt.cids.split('__') : [];

            // startForm.init(countryIds, opt.depart, opt.backward);

            const linkAdd = this.findBlockInside('add-country').findBlockOn('link');
            // todo: передавать имя и параметры маршрута на роутер
            //   вместо хардкор-ссылок
            // либо запрашивать построение ссылок у роутера, передавая
            //   имя и параметры маршрута
            linkAdd.setUrl(`#countries?cids=${countryIds.join('__')}`);

            this.fillSelectedCountries(countryIds);

            // visibility after construction
            this.setMod('visible');
        },
        /**
         * Hide this view
         * @public
         * @returns {undefined}
         */
        deactivate: function() {
            this.delMod('visible');
        },
        /**
         * Fill a block with selected countries
         * @private
         * @param {Array<String>} countryIds IDs of selected countries
         * @returns {undefined}
         */
        fillSelectedCountries: function(countryIds) {
            const sc = this.elem('selected-countries');

            if (!sc) {
                throw new Error('no seleted-countries');
            }

            // clean it
            sc.empty();

            // build a list of selected countries
            api.getCountries().then((arr) => {
                const selectedCountries = arr.filter(item => countryIds.indexOf(item.id) >= 0);

                sc.empty();

                selectedCountries.forEach((i) => {
                    const d = document.createElement('div');
                    d.textContent = i.name;
                    sc.append(d);
                });
            });
        }
    }));
});
