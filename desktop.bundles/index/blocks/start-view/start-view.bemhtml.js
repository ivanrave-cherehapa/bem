block('start-view')(
    js()(true),
    content()([
        {
            elem: 'selected-countries'
        },
        {
            block: 'link',
            mix: 'add-country',
            url: '#countries',
            content: 'Выбор страны'
        },
        {
            content : [
                {
                    block : 'link',
                    url: '#depart',
                    content: 'Дата туда'
                },
                {
                    block : 'link',
                    url: '#backward',
                    content: 'Дата обратно'
                }
            ]
        },
        {
            content: {
                block: 'link',
                url: '#tourists',
                content: 'Путешественники'
            }
        },
        {
            content:
            {
                block: 'link',
                url: '#opt',
                content: 'Опции страховки'
            }
        },
        {
            elem: 'side',
            content: {
                block : 'che-planet'
            }
        }
    ])
);
