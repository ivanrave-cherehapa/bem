({
    shouldDeps: [
        { block: 'api' },
        { block: 'input' },
        { block: 'control-group' },
        { block: 'link' },
        { block: 'che-planet' }
    ]
});
