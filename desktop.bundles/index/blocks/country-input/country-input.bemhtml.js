block('country-input')(
    content()({
        block: 'input',
        mods: {
            width: 'available',
            'has-clear': true,
            // todo: change this theme
            theme: 'islands',
            size: 'xl'
        }
    })
);
