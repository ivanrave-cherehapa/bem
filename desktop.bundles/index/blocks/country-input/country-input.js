/**
 * Фильтр стран
 * Фильтр будет работать только после подгрузки и отображения всех стран.
 * Если возникла ошибка подгрузки данных, то фильтр не работает.
 * Соответственно фильтр зависит:
 * - от блока country-list
 * - от успешной подгрузки стран
 */
modules.define('country-input', ['i-bem__dom'], function(provide, BEMDOM) {
    provide(BEMDOM.decl(this.name, {
        onSetMod: {
            js: {
                inited: function() {
                    // console.log('country-input inited');

                    if (!this.params.target) {
                        throw new Error('target required for country-input, like js: { target: "country-list" }');
                    }

                    const inputBlock = this.findBlockInside('input');

                    if (!inputBlock) {
                        throw new Error('no input inside country-input');
                    }

                    // constructor prop
                    this.inputBlock = inputBlock;

                    // нахождение экземлпяра блока со списком стран
                    //   нахождение через глобальный блок page,
                    //   так как targetBlock может быть где угодно на странице
                    const blockList = this.findBlockOutside('page')
                              .findBlockInside(this.params.target);

                    if (!blockList) {
                        throw new Error('no target block found');
                    }

                    inputBlock.on('change', (ev) => {
                        // todo: не запускать фильтр, если данные не изменились
                        // todo: в случае, если blockList исчезнет со страницы, будет вызвана ошибка
                        blockList.filterList(ev.target.getVal());
                    });

                    // todo: стандартный инпут реагирует на keydown - сменить на keyup
                    // countryInput.addEventListener('change', event => this.handleInputValue(event.target.value));
                    // countryInput.addEventListener('keyup', event => this.handleInputValue(event.target.value));
                }
            }
        },
        /**
         * Clean input value
         * @public
         * @returns {undefined}
         */
        cleanInput: function() {
            // if already initialized
            if (this.inputBlock) {
                this.inputBlock.setVal('');
                this.inputBlock.setMod('focused');
            }
        }
    }));
});
