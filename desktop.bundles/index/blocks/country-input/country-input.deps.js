({
    shouldDeps: [
        { block: 'input' },
        {
            block: 'input',
            mods: {
                width: 'available',
                'has-clear': true,
                theme: 'islands',
                size: 'xl'
            }
        }
    ]
});
