/**
 * Вспомогательные методы:
 *   позволяют строить элементы и модификаторы в модулях
 *   с динамическим созданием ДОМ-элементов.
 * Методы предназначены для сохранения единого стиля посторения
 *   элементов и модификаторов, например, форматы разделителей:
 * - элементы: __
 * - модификаторы: _
 */
modules.define('bm', (provide) => {
    provide({
        /**
         * @param {String} blockName Class name of a parent block
         * @param {String} elemName Element name
         * @returns {String} Full class name for an element
         */
        elem(blockName, elemName) {
            return `${blockName}__${elemName}`;
        },
        /**
         * @param {String} itemName Name of a block or elem
         * @param {String} modName Modificator name
         * @returns {String} Full class name for a modificator
         */
        mod(itemName, modName) {
            return `${itemName}_${modName}`;
        }
    });
});
