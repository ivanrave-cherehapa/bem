block('notfound-view')(
    content()([{
        elem: 'first',
        content: 'Страница не найдена'
    }, {
        elem: 'second',
        content: 'Перейдите на главную'
    }])
);
