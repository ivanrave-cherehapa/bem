/**
 * A main page for a calculator
 */
module.exports = {
    block : 'page',
    title : 'Черехапа страхование',
    head : [
        { elem : 'meta', attrs : { name : 'viewport', content : 'width=device-width, initial-scale=1' } },
        { elem : 'css', url : 'index.min.css' }
    ],
    scripts: [{ elem : 'js', url : 'index.min.js' }],
    content : [{
        elem: 'header',
        content: {
            // a header for all pages: about, calc, contacts
            block: 'che-header'
        }
    }, {
        elem: 'main',
        content: {
            // a main router for a calc page: stored in ./blocks/
            block: 'calc-router'
        }
    }]
};
