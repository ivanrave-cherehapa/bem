module.exports = {
    block : 'page',
    title : 'Главная',
    head : [
        { elem : 'meta', attrs : { name : 'viewport', content : 'width=device-width, initial-scale=1' } },
        { elem : 'css', url : 'home.min.css' }
    ],
    scripts: [{ elem : 'js', url : 'home.min.js' }],
    content : [
        {
            elem: 'header',
            content: {
                block : 'header'
            }
        },
        {
            elem: 'wrap',
            content : [
                {
                    elem: 'country_selection',
                    content: {
                        block : 'input',
                        placeholder : 'Выбор страны'
                    }
                },
                {
                    elem: 'date_selection',
                    content: {
                        block : 'input',
                        placeholder : 'Даты поездки'
                    }
                },
                {
                    elem: 'travelers_info',
                    content: {
                        block : 'input',
                        placeholder : 'Путешественники'
                    }
                },
                {
                    elem: 'insurance_options',
                    content: {
                        block : 'input',
                        placeholder : 'Опции страховки'
                    }
                },
                {
                    block : 'button',
                    mods: {type: 'submit'},
                    text : 'Найти страховку'
                }

            ]
        },
        {
            content: {
                block : 'che-planet'
            }
        }
    ]
};
