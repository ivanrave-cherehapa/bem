block('header')(
    tag()('header'),
    mix() ('clearfix'),
    content()(
        [
            {
                elem: 'menu',
                 content: {
                     block: 'che-menu',
                     mix: {block: 'header', elem: 'menu'}
                 }
            },
            {
                elem: 'logo',
                content:{
                    block: 'link',
                    url: '#',
                    content: {
                        elem: 'logo',
                        tag: 'span'
                    }
                }
            },
            {
                elem: 'phone',
                content: [{
                    elem: 'phone-text',
                    tag: 'span',
                    content: 'Россия (бесплатно)'
                },
                {
                    elem: 'phone-number',
                    content: {
                        block: 'link',
                        url: 'tel:8(800)5552198',
                        content: '8 800 555-21-98'
                    }
                }]
            }
        ]
    )
);
