modules.define('view-router', ['i-bem__dom'], function(provide, BEMDOM) { provide(BEMDOM.decl(this.name, {
    views: [], //массив блоков view, c url или без (например: попапы)
    pages: {}, //блоки view с параметром url

    onSetMod: {
        'js': {
            'inited': function() {
                var views = this.findBlocksInside('view');
                if (!views || !views.length) {
                    return;
                }

                var pages = {};
                views.forEach(function(view) {
                    var url = view.params.url;

                    if (url && !pages[url]) {
                        pages[url] = view;
                    }
                });

                this.views = views;
                this.pages = pages;

                this.bindToWin('hashchange', this.onHashChange);
                this.onHashChange();
            }
        }
    },

    onHashChange: function() {
        this.changeUrl( window.location.hash );
    },

    changeUrl: function(url) {
        var view = this.pages[url];
        if (!view) {
            /* Соглашение:
                первый view является страницей по умолчанию (с пустым url)
                последний view вызывается для обработки всех остальных неизвестных url
            */
            var emptyUrl = (!url || url === '#');
            view = this.views[ emptyUrl ? 0 : (this.views.length-1) ];
        }

        this.changeView(view);
    },

    changeView: function(view) {
        if (this.views.indexOf(view) === -1) {
            return false;
        }

        this.views.forEach(function(otherView) {
            if (otherView !== view) {
                otherView.delMod('visible');
            }
        });
        view.setMod('visible');

        return true;
    }

}))});
