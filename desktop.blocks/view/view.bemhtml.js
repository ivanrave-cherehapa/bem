block('view')(
    tag()('section'),

    attrs()(function() {
        return { id: this.ctx.name };
    }),

    js()(function() {
        var params = {};

        if (this.ctx.name !== undefined) {
            params['url'] = '#' + this.ctx.name;
        }

        if (this.ctx.title !== undefined) {
            params['title'] = this.ctx.title;
        }

        if (this.ctx.header !== undefined) {
            params['header'] = this.ctx.header;
        }

        return params;
    })
)
