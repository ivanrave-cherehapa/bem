modules.define('view', ['i-bem__dom'], function(provide, BEMDOM) { provide(BEMDOM.decl(this.name, {
    _headerBlock: null,
    _footerBlock: null,

    onSetMod: {
        'js': {
            'inited': function() {
                this._headerBlock = this.findBlockOutside('page').findBlockInside('header');
                this._footerBlock = this.findBlockOutside('page').findBlockInside('footer');
            }
        },

        'visible': {
            'true': function() {
                if (this.params.url) {
                    window.location = this.params.url;
                }

                if (this.params.title) {
                    window.document.title = this.params.title;
                }

                //по умолчанию показываем
                this._headerBlock.setMod('visible', (this.params.header !== false));

                //по умолчанию не показываем
                this._footerBlock.setMod('visible', (this.params.footer === true));
            }
        }
    },

}))});
