block('che-header')(
    tag()('header'),
    content()([
        {
            elem: 'main',
            content: [{
                block : 'link',
                url : './',
                content : {
                    block: 'che-logo'
                }
            },{
                block : 'che-phone'
            }
            ]
        },
        {
            elem: 'side',
            content: {
                block: 'che-menu'
            }
        }
    ])
    /*content()(
        [{block : 'dropdown'},
    )*/
);
