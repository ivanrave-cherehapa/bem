({
    shouldDeps: [
        { block: 'dropdown' },
        { block: 'dropdown',
          mods: {
              switcher: 'button',
              theme: 'islands',
              size: 'xl'
          }
        },
        { block: 'button' },
        { block: 'button',
          mods: {
              view: 'plain',
              size: 'xl',
              theme: 'islands'
          }
        },
        { block: 'icon' },
        { block: 'menu' },
        { block: 'menu',
          mods: {
              theme: 'islands',
              size: 'xl',
              autoclosable: true
          }
        },
        { block: 'link' }
    ]
});
