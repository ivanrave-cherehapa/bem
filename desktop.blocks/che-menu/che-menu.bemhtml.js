block('che-menu')(
    content()({
        block : 'dropdown',
        mods : { switcher : 'button', theme : 'islands', size : 'xl' },
        switcher : {
            block: 'button',
            title: 'Меню',
            mods: { theme: 'islands', view: 'plain', size: 'xl' },
            icon: {
                block: 'icon',
                content: '<svg xmlns="http://www.w3.org/2000/svg" width="34" height="34"><path class="menu-icon" d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z"/></svg>'
            }
        },
        popup : {
            block : 'menu',
            tag : 'nav',
            mods : { theme : 'islands', size : 'xl', autoclosable : true },
            content : [
                {
                    block : 'menu-item',
                    content : {
                        block : 'link',
                        url : '#home',
                        content : 'В начало'
                    }
                },
                {
                    block : 'menu-item',
                    content : {
                        block : 'link',
                        url : '#faq',
                        content : 'Вопросы и ответы'
                    }
                },
                {
                    block : 'menu-item',
                    content : {
                        block : 'link',
                        url : '#about',
                        content : 'О компании'
                    }
                }
            ]
        }
    })
);
