// http://eslint.org/docs/user-guide/configuring
// docs for rules at http://eslint.org/docs/rules/{ruleName}
module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true,
        "node": true,
        "bem-xjst/bemhtml": true
    },
    "extends": [
        "eslint:recommended",
        "airbnb-base"
    ],
    "plugins": [
        // https://github.com/bem/eslint-plugin-bem-xjst
        "bem-xjst"
    ],
    "globals": {
        // ymodules global variable
        // todo: find a ymodules plugin
        //   or change a module system, eg. from ymodules to commonjs
        "modules": true
    },
    "rules": {
        // http://eslint.org/docs/rules/brace-style
        // "brace-style": [
        //     "warn",
        //     // https://en.wikipedia.org/wiki/Indent_style#Allman_style
        //     "allman",
        //     { "allowSingleLine": true }
        // ],
        "indent": [
            "error",
            // 4 spaces indentation
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
        "valid-jsdoc": "warn",
        "eol-last": "off",
        "key-spacing": "off",
        "comma-dangle": "off",
        "object-shorthand": "off",
        "global-require": "off",
        "func-names": "off",
        "prefer-arrow-callback": "warn",
        "padded-blocks": "warn",
        "prefer-template": "warn",
        "vars-on-top": "off",
        "max-len": "off",
        "quote-props": "off",
        // deps.js without expressions
        "no-unused-expressions": "off",
        "no-underscore-dangle": "error",
        "no-var": "warn",
        "space-before-function-paren": "off",
        "import/no-extraneous-dependencies": [
            "warn",
            { "devDependencies": true }
        ]
    }
};
